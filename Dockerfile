FROM continuumio/miniconda3:latest

ARG pyver=3.9
ARG build_date=0

ENV TANGO_HOST="nodb:1234"
ENV LIBGL_ALWAYS_SOFTWARE=1
ENV QTWEBENGINE_DISABLE_SANDBOX=1

RUN apt-get update \
    && apt-get install --no-install-recommends -y -q xvfb xauth \
       libqt5webenginecore5 libpci3 libxtst6 \
    && rm -rf /var/lib/apt/lists/*


RUN conda config --add channels conda-forge

RUN conda install --yes python=$pyver 

# use $build_date in the following RUN command to prevent caching of this layer
# (see https://github.com/moby/moby/issues/1996#issuecomment-185872769)
RUN conda install --yes --freeze-installed \
    nomkl \
    click future numpy pint pytango pyepics \
    pyqt pyside2 ply lxml guiqwt pyqtgraph pymca \
    sphinx sphinx_rtd_theme graphviz pyvirtualdisplay \
    pytest pytest-qt pytest-xvfb pytest-forked pytest-xdist flaky \
    && conda clean -afy \
    && find /opt/conda/ -follow -type f -name '*.a' -delete \
    && echo $build_date

